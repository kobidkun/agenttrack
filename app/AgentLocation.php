<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgentLocation extends Model
{
    protected $fillable = [
        'agent_id',
        'latitude',
        'longitude',
        'address',
        'speed',
        'read',
        'is_moving',
    ];
}
