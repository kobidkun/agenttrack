<?php

namespace App\Http\Controllers\Bus;

use App\Http\Requests\BusRegister;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Bus;
class Auth extends Controller
{


    public function busLogin(Request $request)
    {


    }


    public function busRegister(BusRegister $request)
    {


        $newUser = Bus::create($request->all());
        $tokenStr = $newUser->createToken('Token Name')->accessToken;
        return response()->json($tokenStr, 200);

    }


}
