<?php

namespace App\Http\Controllers\Bus;

use App\Bus;
use App\Events\NewMessage;
use App\Events\VelicheLocationUpdate;
use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LRedis;

class ManageBus extends Controller
{
    public function getBusDetails(Request $request)
    {
        $sd = $request->user('bus-api');

        return response()->json(['bus' => $sd]);
    }

    public function PostLocation(Request $request)
    {
        $sd = $request->user('bus-api');
        $getBusid = $sd->bus_id;

        //post bus

        $sv = new Location();
        $sv->sender_id = '123';
        $sv->receiver_id = '456';
        $sv->bus_id = $request->bus_id;
        $sv->latitude = $request->lat;
        $sv->longitude = $request->long;
        $sv->speed = $request->speed;
        $sv->is_moving = 'false';
        $sv->save();
        $data = [
            'bus_id' => $request->bus_id,
            'latitude' => $request->lat,
            'longitude' => $request->long,
            'speed' => $request->speed,
            'is_moving' => 'false',
        ];
        LRedis::publish('test-channel', json_encode($data));
        event(new NewMessage($data));
        return response()->json([
            'status' => 'ok'
        ]);





    }


}
