<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserLocation;
class PrivateMessageController extends Controller
{
    public function getUserNotifications(Request $request)
    {
        $notifications = UserLocation::where('read', 0)
            ->where('receiver_id', $request->user()->id)
            ->orderBy('created_at', 'desc')
            ->get();
        return response(['data' => $notifications], 200);
    }
    public function getPrimateMessages(Request $request)
    {
        $pms = UserLocation::where('receiver_id', $request->user()->id)->orderBy('created_at', 'desc')->get();
        return response(['data' => $pms], 200);
    }
    public function getPrivateMessageById(Request $request)
    {
        $pm = UserLocation::where('id', $request->input('id'))->first();
        // if the message is not read, changing the status
        if ($pm->read == 0) {
            $pm->read = 1;
            $pm->save();
        }
        return response(['data' => $pm], 200);
    }
    public function sendPrivateMessage(Request $request)
    {
        $attributes = [
            'sender_id' => $request->user()->id,
            'receiver_id' => $request->input('receiver_id'),
            'message' => $request->input('message'),
            'subject' => $request->input('subject'),
            'read' => 0,
        ];
        $pm = UserLocation::create($attributes);
        $data = UserLocation::where('id', $pm->id)->first();
        return response(['data' => $data], 201);
    }
    public function getPrivateMessageSent(Request $request)
    {
        $pms = UserLocation::where('sender_id', $request->user()->id)->orderBy('created_at', 'desc')->get();
        return response(['data' => $pms], 200);
    }
}
