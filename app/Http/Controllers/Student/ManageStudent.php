<?php

namespace App\Http\Controllers\Student;

use App\Bus;
use App\Location;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Student;

class ManageStudent extends Controller
{



    public function __construct()
    {
        $this->middleware('auth:student-api');
    }



    public function getStudentsDetails(Request $request)
    {
        $sd = $request->user();

        $gsid = $sd->bus_id;
        $findstuent = Student::find($gsid);
        $fbusid = Bus::findorfail($gsid);


        return response()->json(['student' => $sd, 'bus' => $fbusid]);
    }

    public function getBusLocation(Request $request)
    {
        $sd = $request->user('student-api');

        $gsid = $sd->bus_id;
        $lastlocation = Location::where('bus_id', '=', $gsid)->firstOrFail()->orderBy('created_at', 'desc')->first();
        $latitude = $lastlocation->latitude;
        $longitude = $lastlocation->longitude;
        $speed = $lastlocation->speed;
        $ismoving = $lastlocation->is_moving;


        return response()->json([
            'latitude' => $latitude,
            'longitude' => $longitude,
            'speed' => $speed,
            'ismoving' => $ismoving
        ], 200);
    }
}
