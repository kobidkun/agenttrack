<?php

namespace App\Http\Controllers\agent;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agent;
class AgentAuthController extends Controller
{
    public function AgentRegister( $request)
    {


        $newUser = Agent::create($request->all());
        $tokenStr = $newUser->createToken('Token Name')->accessToken;
        return response()->json($tokenStr, 200);

    }

    public function AgentRegisterweb()
    {


        return view('agent.register');

    }

    public function AgentRegisterwebsave( Request $request)
    {
        $a = new Agent();
        $a->agent_id = $request->agent_id;
        $a->senior_id = $request->senior_id;
        $a->name = $request->name;
        $a->designation = $request->designation;
        $a->address = $request->address;
        $a->city = $request->city;
        $a->state = $request->state;
        $a->location_share_id = $request->location_share_id;
        $a->email = $request->email;
        $a->mobile = $request->mobile;

        $a->office_address = $request->office_address;
        $a->office_lat = $request->office_lat;
        $a->office_long = $request->office_long;
        $a->password = bcrypt($request->password);
        $a->save();
        return back();

    }







}
