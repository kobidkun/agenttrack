<?php

namespace App\Listeners;

use App\Events\VelicheLocationUpdate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateLocationtoMobile
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  VelicheLocationUpdate $event
     * @return void
     */
    public function handle(VelicheLocationUpdate $event)
    {
        //
    }
}
