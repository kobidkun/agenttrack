<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Location extends Model
{


    protected $fillable = [
        'bus_id',
        'latitude',
        'longitude',
        'address',
        'speed',
        'read',
        'is_moving'
    ];


    public function bus()
    {
        return $this->belongsTo('App\Bus');
    }
}
