@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Agent Register</div>

                <div class="panel-body" >
                    <form class="form-horizontal" method="POST" action="{{ route('user.register.web.save') }}" style="padding: 10px 10px 10px 10px;">
                        {{ csrf_field() }}


                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent id</label>
                            <input type="text" class="form-control"
                                   id="exampleInputEmail1"
                                   name="agent_id"
                                   aria-describedby="emailHelp"
                                   placeholder="Agent id">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Designation</label>
                            <input type="text" class="form-control"
                                   id="exampleInputEmail1"
                                  name="designation"
                                   aria-describedby="emailHelp"
                                   placeholder="Agent Designation">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Address</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="address"
                                   placeholder="Agent Address">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="name"
                                   placeholder="Agent Name">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent City</label>
                            <input type="text"
                                   name="city"
                                   class="form-control"
                                   id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   placeholder="Agent City">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent State</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="state"
                                   placeholder="Agent State">
                        </div>


                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Mobile</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="mobile"
                                   placeholder="Agent Mobile">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Office Address</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="office_address"
                                   placeholder="Agent Office Address">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Office Latitude</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="office_lat"
                                   placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Office Longitude</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="office_long"
                                   placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Email</label>
                            <input type="text" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="email"
                                   placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Agent Password</label>
                            <input type="password" class="form-control" id="exampleInputEmail1"
                                   aria-describedby="emailHelp"
                                   name="password"
                                   placeholder="Enter email">
                        </div>



                        <button type="submit" class="btn btn-primary">Submit</button>








                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
