<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/user2', function (Request $request) {
    return $request->user('api');
});


Route::get('/admin', function (Request $request) {
    return $request->user('api');
});




//agennts


Route::post('agent/auth/register', 'agent\AgentAuthController@AgentRegister');
Route::get('agent/profile', 'agent\AgentManage@GetAgentProfile');






//agennts end


/*//bus functionds //student-api
Route::post('bus/auth/register', 'bus\Auth@busRegister');
Route::get('bus/getdetails', 'Bus\ManageBus@getBusDetails')->middleware('auth:api');
Route::post('bus/sendlocation', 'Bus\ManageBus@PostLocation')->middleware('auth:api');
//bus functionds //
//
///
///
///   //student functionds //
Route::post('student/auth/register', 'Student\AuthController@studentRegister');


//students post
Route::middleware('auth:api')->get('student/auth/login', function (Request $request) {
    return $request->user('student-api');
});

Route::get('student/getdetails', 'Student\ManageStudent@getStudentsDetails')->middleware('auth:api');
Route::get('student/getbuslocation', 'Student\ManageStudent@getBusLocation')->middleware('auth:api');
//student functionds //*/