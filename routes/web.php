<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('policy');
});

Route::get('test', function () {
    // Route logic...
    $data = array("id" => "1", "name" => "jobin", "amount" => "1000");
    LRedis::publish('test-channel', json_encode($data));
    event(new App\Events\NewMessage($data));
    return "event fired";
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::post('agent/auth/register', 'agent\AgentAuthController@AgentRegisterwebsave')->name('user.register.web.save');
Route::get('agent/auth/register', 'agent\AgentAuthController@AgentRegisterweb')->name('user.register.web');

